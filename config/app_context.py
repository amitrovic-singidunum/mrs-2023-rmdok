class AppContext:
    def __init__(self, activated_plugins=[], workspace_path=""):
        self.activated_plugins = activated_plugins
        self.workspace_path = workspace_path

    @staticmethod
    def from_dict(context_dict):
        activated_plugins = context_dict.get("activated_plugins", [])
        workspace_path = context_dict.get("workspace_path", "")
        return AppContext(activated_plugins, workspace_path)
    
    def to_dict(self):
        return {
            "activated_plugins": self.activated_plugins,
            "workspace_path": self.workspace_path
        }